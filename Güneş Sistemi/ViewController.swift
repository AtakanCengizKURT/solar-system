//
//  ViewController.swift
//  Güneş Sistemi
//
//  Created by MacBook Pro on 14.09.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        
//        let mySun = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0.01)
        let node = createSphere(radius: 0.1, content: "sun.png", SCNVector: SCNVector3(0, 0.1, -1))
        let mars = createSphere(radius: 0.1, content: "mars.png", SCNVector: SCNVector3(0, 0.13, -1.3))
        let earth = createSphere(radius: 0.1, content: "earth.png", SCNVector: SCNVector3(0, 0.16, -1.6))
        let tropical = createSphere(radius: 0.1, content: "tropical.png", SCNVector: SCNVector3(0, 0.19, -1.9))
        let venus = createSphere(radius: 0.1, content: "venus.png", SCNVector: SCNVector3(0, 0.22, -2.2))
        let gas = createSphere(radius: 0.1, content: "gas.png", SCNVector: SCNVector3(0, 0.25, -2.5))
        
        sceneView.scene.rootNode.addChildNode(node)
        sceneView.scene.rootNode.addChildNode(mars)
        sceneView.scene.rootNode.addChildNode(earth)
        sceneView.scene.rootNode.addChildNode(tropical)
        sceneView.scene.rootNode.addChildNode(venus)
        sceneView.scene.rootNode.addChildNode(gas)
        sceneView.automaticallyUpdatesLighting = true
    }

    func createSphere(radius: CGFloat, content: String, SCNVector: SCNVector3)-> SCNNode{
        let mySun = SCNSphere(radius: radius)
             let sunMaterial = SCNMaterial()
             sunMaterial.diffuse.contents = UIImage(named: "art.scnassets/\(content)")
             mySun.materials = [sunMaterial]
             let node = SCNNode()
             node.position = SCNVector
             node.geometry = mySun
        return node
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
